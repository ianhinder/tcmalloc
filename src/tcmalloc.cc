
// #include <stdio.h> 
// #include <string.h> 
// #include <sys/resource.h>
// #include <unistd.h>
// #include <stdlib.h>
// #include <errno.h>
// #include "assert.h"

#include <sstream>

#include "cctk.h" 
// #include "cctk_Arguments.h" 
#include "cctk_Parameters.h" 

#include "google/malloc_extension.h"
#include "google/heap-profiler.h"

#define COLLECT_PROPERTY(group,item) \
  if (MallocExtension::instance()->GetNumericProperty(#group "." #item, &val)) { \
    *group##_##item = (CCTK_REAL) val; \
  } \
  else { \
    CCTK_VError(__LINE__, __FILE__, CCTK_THORNSTRING, "Error retrieving property %s", #group "." #item); \
  }

extern "C" void tcmalloc_Collect(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  // if (sizeof(size_t) != 8) {
  //   CCTK_VError(__LINE__, __FILE__, CCTK_THORNSTRING, "Expecting sizeof(size_t) == 8 but found %d", sizeof(size_t));
  // }

  size_t val;
  
  // if (MallocExtension::instance()->GetNumericProperty("generic.current_allocated_bytes", &val)) {
  //   *generic_current_allocated_bytes = (CCTK_REAL) val;
  // }
  // else {
  //   CCTK_VError(__LINE__, __FILE__, CCTK_THORNSTRING, "Error retrieving property %s", "generic.current_allocated_bytes");
  // }

  COLLECT_PROPERTY(generic,  current_allocated_bytes);
  COLLECT_PROPERTY(generic,  heap_size);
  COLLECT_PROPERTY(tcmalloc, pageheap_free_bytes);
  COLLECT_PROPERTY(tcmalloc, pageheap_unmapped_bytes);
  COLLECT_PROPERTY(tcmalloc, slack_bytes);
  COLLECT_PROPERTY(tcmalloc, max_total_thread_cache_bytes);
  COLLECT_PROPERTY(tcmalloc, current_total_thread_cache_bytes);
}

extern "C" void tcmalloc_Report(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (report_every != -1 && cctk_iteration % report_every == 0) {
    char buffer[1024*10];
    MallocExtension::instance()->GetStats(buffer, sizeof(buffer));
    CCTK_VInfo(CCTK_THORNSTRING, "Memory statistics:\n%s", buffer);
  }
}

extern "C" void tcmalloc_ReleasePeriodically(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (release_every != -1 && cctk_iteration % release_every == 0) {
    CCTK_VInfo(CCTK_THORNSTRING, "Releasing free memory to OS");
    MallocExtension::instance()->ReleaseFreeMemory();
  }
}

extern "C" void tcmalloc_Release(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_VInfo(CCTK_THORNSTRING, "Releasing free memory to OS");
  MallocExtension::instance()->ReleaseFreeMemory();
}

extern "C" void tcmalloc_ProfileStart(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_PARAMETERS;

  if (output_profile_every != -1) {
    std::stringstream prefix;
    prefix << "heap." << CCTK_MyProc(cctkGH);
    HeapProfilerStart(prefix.str().c_str());
  }
}

extern "C" int tcmalloc_ProfileStop()
{
  DECLARE_CCTK_PARAMETERS;

  if (output_profile_every != -1) {
    HeapProfilerStop();
  }
  return 0;
}

extern "C" void tcmalloc_Profile(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (output_profile_every != -1 && cctk_iteration % output_profile_every == 0) {
    std::stringstream reason;
    reason << "Iteration " << cctk_iteration;
    CCTK_VInfo(CCTK_THORNSTRING, "Writing heap profile at iteration %d", cctk_iteration);
    HeapProfilerDump(reason.str().c_str());
  }
}
